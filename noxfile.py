# Copyleft 2024 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

import nox


@nox.session(reuse_venv=True)
def flake8(session):
    session.install("flake8")
    session.run("flake8", "--max-line-length=80", "--ignore=E402,W503", "fox")


@nox.session(reuse_venv=True)
def black(session):
    session.install("black")
    session.run(
        "black",
        "--check",
        "--diff",
        "--color",
        "--line-length=80",
        "fox",
        "noxfile.py",
    )


@nox.session(reuse_venv=True)
def black_apply(session):
    session.install("black")
    session.run("black", "--line-length=80", "fox", "noxfile.py")
