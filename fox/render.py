# Copyleft 2024 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, field
from datetime import date
from html import escape
from pathlib import Path
from re import compile as Compile, Match, search, sub

try:
    from pygments import highlight
    from pygments.lexers import get_lexer_by_name
    from pygments.formatters import HtmlFormatter

    pygments_available = True
except ImportError:
    pygments_available = False

from fox.parser import gemini_parser


@dataclass
class GeminiRender:
    """Renderer the missing parts from the Gemini document

    Notes
    -----
    The goal is to have correct base URL and the permalinks section.

    Attributes
    ----------
    base_url : str
        The HTML base URL used to generate the link
    gemini_url : str
        The Gemini base URL used to generate the link
    """

    base_url: str = "/"
    gemini_url: str = "/"

    def render(self, path: Path, content: list[str]) -> dict[str:str]:
        """Rendered the content

        Parameters
        ----------
        path : pathlib.Path
            The relative path of the Gemini file
        content : list
            The list of lines from the Gemini file

        Returns
        -------
        dict
            The rendered content with the body and slug used to generate a
            new version of the GemText.
        """

        slug = path.stem

        # Parse the date from the name and prepare the HTML inline block
        if output := search(r"^(\d{4}-\d{2}-\d{2})-", path.stem):
            # Remove the date from the filename to have the same slug as before
            slug = slug.removeprefix(f"{output.group(1)}-")

        body = ""
        for line in content:
            # Replace the base URL to have correct links
            line = sub(r"^=>\s+/", f"=> {self.gemini_url}", line)

            body += f"{line}\n"

        # Generate the permalinks section
        body += "\n".join(
            [
                "\n–––",
                "Liens permanents :",
                f"=> {self.gemini_url}{path.with_name(f'{slug}.gmi')}",
                f"=> {self.base_url}{path.with_name(f'{slug}.html')}",
            ]
        )

        return {
            "body": body,
            "slug": slug,
        }


@dataclass
class HTMLRender:
    """Renderer a Gemini parsed content as a HTML webpage

    Notes
    -----
    I want to keep the rendered body as simple as possible. The additional
    features as the Gemini link marker and the colored pre blocks are only
    here to help the reader.

    The goal is also to stay readable on an emulator of terminal with w3m
    or elinks.

    Attributes
    ----------
    base_url : str
        The HTML base URL used to generate the link
    gemini_url : str
        The Gemini base URL used to generate the link
    style : str
        The style used with Pygment
    enable_atom : bool
        Activate the generation of the Atom feed link
    enable_rss : bool
        Activate the generation of the RSS feed link
    html_colors : list
        The list of 4 colors to use with the HTML template
    """

    base_url: str = "/"
    gemini_url: str = "/"
    style: str = "default"
    enable_atom: bool = False
    enable_rss: bool = False
    html_colors: list[str] = field(default_factory=list)

    # Store the footnotes to avoid convert an unknown number
    footnotes: list[str] = field(default_factory=list, init=False)

    def __post_init__(self):
        """Store the compiled version of the RegEx"""

        self.re_date_link = Compile(r"^(\d{4}-\d{2}-\d{2})")
        self.re_footnote = Compile(r"(^)\[(\d+)\]")

    def __args__(self, **kwargs: dict[str, str]) -> str:
        """Generate the list of attributes for a block

        Parameters
        ----------
        kwargs : dict
            The attributes to add as a dictionnary

        Returns
        -------
        str
            The string with each item from the kwargs as “key="value"”
        """

        return "".join([f' {key}="{value}"' for key, value in kwargs.items()])

    def __block__(self, block: str, text: str, **kwargs: dict[str, str]) -> str:
        """Represents a block element with new lines for the source

        Parameters
        ----------
        block : str
            The name of the block
        text : str
            The content of the block
        kwargs : dict
            The attributes of the block

        Returns
        -------
        str
            The rendered block as string
        """

        return f"<{block}{self.__args__(**kwargs)}>\n{text}\n</{block}>\n"

    def __inline__(
        self, block: str, text: str, **kwargs: dict[str, str]
    ) -> str:
        """Represents an inline block element

        Parameters
        ----------
        block : str
            The name of the block
        text : str
            The content of the block
        kwargs : dict
            The attributes of the block

        Returns
        -------
        str
            The rendered block as string
        """

        return f"<{block}{self.__args__(**kwargs)}>{text}</{block}>"

    def __inline_block__(
        self, block: str, text: str, **kwargs: dict[str, str]
    ) -> str:
        """Represents an inline block element with new line for the source

        Parameters
        ----------
        block : str
            The name of the block
        text : str
            The content of the block
        kwargs : dict
            The attributes of the block

        Returns
        -------
        str
            The rendered block as string
        """

        return f"{self.__inline__(block, text, **kwargs)}\n"

    def __tag__(self, block: str, **kwargs: dict[str, str]) -> str:
        """Represents a tag element

        Parameters
        ----------
        block : str
            The name of the tag
        kwargs : dict
            The attributes of the tag

        Returns
        -------
        str
            The rendered tag as string
        """

        return f"<{block}{self.__args__(**kwargs)} />"

    def __tag_block__(self, block: str, **kwargs: dict[str, str]) -> str:
        """Represents a tag element with new line for the source

        Parameters
        ----------
        block : str
            The name of the tag
        kwargs : dict
            The attributes of the tag

        Returns
        -------
        str
            The rendered tag as string
        """

        return f"{self.__tag__(block, **kwargs)}\n"

    def __a__(
        self, url: str, alt_text: str = "", parse_date: bool = False
    ) -> str:
        """Represents the <a> inline block element

        Parameters
        ----------
        url : str
            The URL of the resource for this link
        alt_text : str
            The alternative text to use for this link
        parse_date : bool
            True if the date at the start of the URL must be separated

        Returns
        -------
        str
            The rendered block as string
        """

        text = ""

        if url.startswith("/"):
            # Only replace the Gemini links when there are from my website
            if url.endswith(".gmi"):
                url = url.replace(".gmi", ".html")

            # Replace the base URL to have correct links
            url = f"{self.base_url}{url[1:]}"

        # Use the URL as alternative text if not specified
        if len(alt_text.split()) == 0:
            alt_text = url

        # I want to separate the date from the link to have a pretier format
        if parse_date:
            if output := self.re_date_link.search(alt_text):
                str_date = output.group(1)
                format_date = date.fromisoformat(str_date).strftime("%d %B")

                time = self.__inline__("time", format_date, datetime=str_date)
                text += self.__inline__("small", f"{time} / ")

                alt_text = alt_text.removeprefix(str_date)

        text += self.__inline__("a", alt_text.strip(), href=url.strip())

        # This rocket marker is usefull to have a distinction between HTTP and
        # Gemini links in the HTML body render
        if url.startswith("gemini://"):
            gemini_text = self.__inline__("span", "🚀", title="Capsule Gemini")
            text += f" {gemini_text}"
        # The same behavior with the email URI
        elif url.startswith("mailto:"):
            gemini_text = self.__inline__(
                "span", "📧", title="Adresse courriel"
            )
            text += f" {gemini_text}"

        return text

    def __code__(self, output: Match) -> str:
        """Represents the <code> block element

        Notes
        -----
        This method is used by the re.sub function. This is why the first
        parameters is a Match object.

        Parameters
        ----------
        output : re.Match
            The content of the block

        Returns
        -------
        str
            The rendered block as string
        """

        return self.__inline__("code", escape(output.group(1)))

    def __footnote__(self, output: Match) -> str:
        """Represents a footnote <a> inline block element

        Notes
        -----
        This method is used by the re.sub function. This is why the first
        parameters is a Match object.

        Parameters
        ----------
        output : re.Match
            The content of the block

        Returns
        -------
        str
            The rendered block as string
        """

        # Link in the footnote section
        if len(output.groups()) == 2:
            index = output.group(2)
            text = index
            url = f"#ft{index}"

            self.footnotes.append(output.group(2))

        # Link inside the content
        elif output.group(1) in self.footnotes:
            index = f"ft{output.group(1)}"
            text = self.__inline__("sub", output.group(1))
            url = f"#{output.group(1)}"

        # Avoid to convert
        else:
            return f"[{output.group(1)}]"

        return self.__inline__("a", text, href=url, id=index)

    def __permalinks__(self, links: list[str]) -> str:
        """Represents the permalinks section for Gemini and HTML links

        Notes
        -----
        Remove the last character of the block to avoid an unnecessary new line

        Parameters
        ----------
        links : list[str]
            The list of permalinks

        Returns
        -------
        str
            The rendered block as string
        """

        return self.__block__(
            "aside",
            self.__inline__(
                "small", "\n".join(["Liens permanents :", self.__ul__(links)])
            ),
        )[:-1]

    def __pre__(self, code_type: str, lines: str) -> str:
        """Represents the <pre> block element

        Notes
        -----
        If pygments if available on the system, the code will be highlighted.

        Parameters
        ----------
        code_type : str
            The language used by the block
        lines : str
            The content of the block

        Returns
        -------
        str
            The rendered block as string
        """

        text = "\n".join(lines)

        # The language used by the code block is mandatory to use pygments
        if len(code_type) > 0 and pygments_available:
            text = highlight(
                text,
                get_lexer_by_name(code_type),
                HtmlFormatter(noclasses=True, nowrap=True, style=self.style),
            )
        else:
            text = escape(text)

        return self.__inline_block__("pre", text)

    def __ul__(self, items: list[str]) -> str:
        """Represents the ul block element

        Parameters
        ----------
        items : list
            The <li> inline block to add in the <ul> list

        Returns
        -------
        str
            The rendered block as string
        """

        return self.__block__(
            "ul", "\n".join([self.__inline__("li", text) for text in items])
        )

    def render(self, path: Path, content: list[str]) -> dict[str:str]:
        """Rendered the content to be used by the HTML template

        Parameters
        ----------
        path : pathlib.Path
            The relative path of the Gemini file
        content : list
            The list of lines from the Gemini file

        Returns
        -------
        dict
            The rendered content with the body, date and title which can be
            used with the HTML template.
        """

        body = ""
        real_date = ""
        page_date = ""
        page_title = path.stem
        slug = path.stem

        self.footnotes.clear()

        for block, data in gemini_parser(content):
            if block == "a":
                # I like to have the links in a structured <ul> list, like the
                # behavior with the Gemini link markup
                body += self.__ul__(
                    [
                        self.__a__(*element, parse_date=path.stem == "index")
                        for element in data
                    ]
                )

            elif block == "blockquote":
                # Use the <br /> to ensure multi-lines quotes to keep the same
                # density (useful for poems)
                body += self.__block__("blockquote", "<br />".join(data))

            elif block.startswith("h"):
                # The first header will be used by the template
                if block == "h1":
                    page_title = data
                # The other headers can be shown in the body
                else:
                    body += self.__inline_block__(block, data)

            elif block == "image":
                src_url = data[0]

                # Replace the base URL to have correct links
                if src_url.startswith("/"):
                    src_url = f"{self.base_url}{src_url[1:]}"

                options = {"src": src_url}
                if len(data[1]) > 0:
                    options["alt"] = data[1]
                    options["title"] = data[1]
                # Use the figure block to allow futher design customization
                # (and maybe caption later?)
                body += self.__block__("figure", self.__tag__("img", **options))

            elif block == "li":
                body += self.__ul__(data)

            elif block == "p":
                # Case where I want to replace the triple tirets with <hr />
                if data == "–––":
                    body += self.__tag_block__("hr")

                else:
                    # This is a footnote
                    if output := self.re_footnote.search(data):
                        data = self.re_footnote.sub(self.__footnote__, data)

                    body += self.__inline_block__("p", data)

            elif block == "pre":
                body += self.__pre__(*data)

        # Replace code part from the generated body
        body = sub(r"`([^`]+)`", self.__code__, body)
        # Replace footnote part from the generated body
        body = sub(r"\[(\d+)\]", self.__footnote__, body)

        # Parse the date from the name and prepare the HTML inline block
        if output := search(r"^(\d{4}-\d{2}-\d{2})-", path.stem):
            real_date = date.fromisoformat(output.group(1))

            formated = real_date.strftime("%d %B %Y")
            page_date = self.__inline_block__("small", f"Publié le {formated}")

            # Remove the date from the filename to have the same slug as before
            slug = slug.removeprefix(f"{output.group(1)}-")

        # Generate the permalinks section
        permalinks = self.__permalinks__(
            [
                self.__a__(f"{self.gemini_url}{path.with_name(f'{slug}.gmi')}"),
                self.__a__(f"{self.base_url}{path.with_name(f'{slug}.html')}"),
            ]
        )

        # Generate the feed links section
        feeds = ""

        if self.enable_atom:
            block = self.__tag__(
                "link",
                href=f"{self.base_url}feed/atom.xml",
                rel="alternate",
                title="Flux Atom",
                type="application/atom+xml",
            )
            feeds += f"\n{block}"

        if self.enable_rss:
            block = self.__tag__(
                "link",
                href=f"{self.base_url}feed/rss.xml",
                rel="alternate",
                title="Flux RSS",
                type="application/rss+xml",
            )
            feeds += f"\n{block}"

        # Generate the CSS colors section
        colors = [
            f"--c{index}:{color}"
            for index, color in enumerate(self.html_colors)
        ]

        return {
            "body": body[:-1],
            "colors": f":root{{{';'.join(colors)}}}",
            "date": real_date,
            "feeds": feeds,
            "page_date": page_date,
            "permalinks": permalinks,
            "slug": slug,
            "title": page_title,
        }
