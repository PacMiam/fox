# Copyleft 2024 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from re import compile as Compile
from typing import Any


class GeminiSpecification:
    """RegEx patterns for the Gemini Text Specification

    Notes
    -----
    I’ve made a distinction between the LINK and the MEDIA to allow different
    rendering with the HTML parser.

    See Also
    --------
    https://geminiprotocol.net/docs/specification.gmi
    """

    HEADER = Compile(r"^(#{1,6})\s(.+)$")
    MEDIA = Compile(r"^=>[\s\t]*([^\.]+\.(?:jpg|png|webp))\s*(.*)$")
    LINK = Compile(r"^=>[\s\t]*([^\s]+)\s*(.*)$")
    LIST = Compile(r"^\* (.+)$")
    PREFORMAT = Compile(r"^```(.*)$")
    QUOTE = Compile(r"^>\s*(.*)$")


def parse_line(line: str) -> tuple[str, Any]:
    """Detect the type of markup for the specified line

    Parameters
    ----------
    line : str
        The line to check with each Gemini Text Specification

    Returns
    -------
    (str, Any)
        A tuple with the HTML block associate with the Gemini Text Specification
        and the content of this block

    Examples
    --------
    >>> parse_line("=> test.png This is a test")
    ("img", ("test.png", "This is a test"))
    """

    if data := GeminiSpecification.HEADER.search(line):
        return (f"h{len(data.group(1))}", data.group(2))

    if data := GeminiSpecification.MEDIA.search(line):
        return ("image", data.groups())

    if data := GeminiSpecification.LINK.search(line):
        return ("a", data.groups())

    if data := GeminiSpecification.LIST.search(line):
        return ("li", data.group(1))

    if data := GeminiSpecification.PREFORMAT.search(line):
        return ("pre", data.group(1))

    if data := GeminiSpecification.QUOTE.search(line):
        return ("blockquote", data.group(1))

    return ("p", line)


def gemini_parser(lines: list[str]) -> list[list[str, Any]]:
    """Parse the content of the lines from the Gemini file

    Parameters
    ----------
    lines : [str]
        The lines from the content of the Gemini file

    Returns
    -------
    list
        The content of the file as a structured list which contains the HTML
        block and the associated data for each line

    Examples
    --------
    >>> content = '''# PacMiam
    This is my capsule :D
    => https://www.gnu.org GNU website
    '''
    >>> gemini_parser(content.splitlines())
    [["h1", "PacMiam"],
     ["p", "This is my capsule :D"],
     ["a", ["https://www.gnu.org", "GNU website"]]]
    """

    structure = []

    previous_block = None
    for line in lines:
        # This is an empty new line
        if len(line.strip()) == 0:
            # It’s normal to find empty lines in a preformatted block
            if previous_block == "pre":
                continue
            # Ensure to create another block when a new line occurs
            elif previous_block in ("a", "blockquote", "li"):
                previous_block = None
                continue

        block, data = parse_line(line)

        # Start the preformatted block
        if block == "pre" and not previous_block == "pre":
            structure.append([block, [data, []]])
        # Still in the preformatted block
        elif not block == "pre" and previous_block == "pre":
            structure[-1][-1][-1].append(line)
            previous_block = "pre"
            continue
        # This is the end… for the preformatted block
        elif block == "pre" and previous_block == "pre":
            previous_block = None
            continue

        # Inline elements
        elif block.startswith("h") or block in ("image", "p"):
            structure.append([block, data])

        # Block elements
        elif block in ("a", "blockquote", "li"):
            if block == previous_block:
                structure[-1][-1].append(data)
            else:
                structure.append([block, [data]])

        previous_block = block

    return structure
