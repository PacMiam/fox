# Copyleft 2024 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from csv import DictReader
from dataclasses import dataclass, field
from datetime import datetime
from html import escape
from importlib.metadata import version
from pathlib import Path
from re import sub
from string import Template
from uuid import NAMESPACE_URL, uuid5

from fox.render import GeminiRender, HTMLRender

HTML_COLORS = ["#5b3109", "#846b29", "#ce9c85", "#ffe7c5"]


@dataclass
class Taniere:
    """Static generator from my gemini files

    Attributes
    ----------
    base_url : str
        The URL used for the inner links in the rendered HTML
    gemini_url : str or None
        The Gemini URL used for permalinks if different from the base_url
    content_path : pathlib.Path
        The path to my gemini articles and pages
    output_path : pathlib.Path
        The path to the output directory where the HTML pages will be generated
    template_path : pathlib.Path
        The path to the HTML template directory
    datetime_path : pathlib.Path or None
        The path to the CSV file which contains the full datetime of each
        articles for the RSS and Atom renderer
    style : str
        The Pygments style to use if this module was loaded
    templates : list
        The list of template used to render basic HTML page and XML feeds
    author : str
        The name of the author of the website
    description : str
        The description of the website
    keywords : str
        The list of comma separated keyword for this website
    title : str
        The title of the website used to render the <title> HTML block
    year : str
        The current year used with the copyleft footer
    html_colors : list
        The list of 4 colors to use with the default HTML template
    enable_atom : bool
        Activate the generation of the Atom feed from the available articles
    enable_rss : bool
        Activate the generation of the RSS feed from the available articles
    """

    base_url: str
    gemini_url: str | None
    content_path: Path
    output_path: Path
    template_path: Path
    style: str

    author: str
    datetime_path: Path | None
    description: str
    keywords: str
    title: str
    year: str

    html_colors: list[str] = field(default_factory=list)

    enable_atom: bool = False
    enable_rss: bool = True

    gemini_render: GeminiRender = field(init=False)
    html_render: HTMLRender = field(init=False)
    templates: dict[str, Template] = field(default_factory=dict, init=False)
    datetimes: dict[str, datetime] = field(default_factory=dict, init=False)

    # Store the Fox version from the pyproject.toml file
    VERSION = version(__package__)

    def __post_init__(self):
        """Check the path specified by the constructor"""

        if not self.content_path.exists():
            raise FileNotFoundError(
                f"Cannot found {self.content_path} on the filesystem"
            )

        if not self.output_path.exists():
            self.output_path.mkdir(mode=0o700, parents=True)

        if self.gemini_url is None:
            self.gemini_url = sub(r"^https?", "gemini", self.base_url)

        self.gemini_render = GeminiRender(self.base_url, self.gemini_url)
        self.html_render = HTMLRender(
            base_url=self.base_url,
            gemini_url=self.gemini_url,
            style=self.style,
            enable_atom=self.enable_atom,
            enable_rss=self.enable_rss,
            html_colors=self.html_colors,
        )

        self.templates = {
            "base": Template(
                self.template_path.joinpath("base.html").read_text()
            ),
        }

        if self.enable_atom:
            self.templates["atom"] = Template(
                self.template_path.joinpath("atom.xml").read_text()
            )
            self.templates["atom-entry"] = Template(
                self.template_path.joinpath("atom-entry.xml").read_text()
            )

        if self.enable_rss:
            self.templates["rss"] = Template(
                self.template_path.joinpath("rss.xml").read_text()
            )
            self.templates["rss-entry"] = Template(
                self.template_path.joinpath("rss-entry.xml").read_text()
            )

        # Generate the subdirectories
        self.html_output_path = self.output_path.joinpath("html")
        self.gemini_output_path = self.output_path.joinpath("gemini")

        # Retrieve the datetime of the articles if the file exists
        if (
            self.datetime_path is not None
            and self.datetime_path.exists()
            and self.datetime_path.is_file()
        ):
            with self.datetime_path.open() as file_buffer:
                self.datetimes = {
                    row["filename"]: datetime.fromisoformat(row["datetime"])
                    for row in DictReader(file_buffer)
                }

    def __iter__(self) -> list[Path]:
        """Iterate through the Gemini file in the content directory

        Returns
        -------
        list
            The list of Gemini file path object
        """

        return self.content_path.rglob("*.gmi")

    def generate_page(self, page: Path):
        """Generate the specified Gemini page as a HTML web page

        Parameters
        ----------
        page : pathlib.Path
            The page to parse and render
        """

        rel_path = page.relative_to(self.content_path)
        content = page.read_text().splitlines()

        # Gemini
        renderer = self.gemini_render.render(rel_path, content)

        slug = renderer["slug"]
        url = rel_path.with_name(f"{slug}.gmi")

        path = self.gemini_output_path.joinpath(url)
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(renderer["body"])

        # HTML
        renderer = self.html_render.render(rel_path, content)

        slug = renderer["slug"]
        url = rel_path.with_name(f"{slug}.html")

        path = self.html_output_path.joinpath(url)
        path.parent.mkdir(parents=True, exist_ok=True)

        title = renderer["title"]
        if not title == self.title:
            title = f"{title} – {self.title}"

        path.write_text(
            self.templates["base"].substitute(
                {
                    "author": self.author,
                    "base_url": self.base_url,
                    "body": renderer["body"],
                    "colors": renderer["colors"],
                    "date": renderer["page_date"],
                    "description": self.description,
                    "feeds": renderer["feeds"],
                    "fox_version": self.VERSION,
                    "keywords": self.keywords,
                    "page_title": renderer["title"],
                    "permalinks": renderer["permalinks"],
                    "style": self.template_path.joinpath(
                        "style.css"
                    ).read_text()[:-1],
                    "title": title,
                    "url": url,
                    "year": self.year,
                }
            )
        )

    def generate_atom_feed(self, max_articles: int = 10):
        """Generate the Atom XML feed from a specific amount of articles

        Parameters
        ----------
        max_articles : int
             The number of articles to show in the Atom feed
        """

        articles = sorted(
            self.content_path.joinpath("articles").glob("2*.gmi"),
            reverse=True,
        )

        content = []
        for path in articles[0:max_articles]:
            rel_path = path.relative_to(self.content_path)
            renderer = self.html_render.render(
                rel_path, path.read_text().splitlines()
            )

            if (article_date := self.get_article_datetime(path.name)) is None:
                article_date = renderer["date"]

            slug = renderer["slug"]
            path = rel_path.with_name(f"{slug}.html")

            output = self.templates["atom-entry"].substitute(
                {
                    "author": self.author,
                    "base_url": self.base_url,
                    "body": escape(renderer["body"]),
                    "date": article_date.isoformat(),
                    "slug": slug,
                    "title": renderer["title"],
                    "uuid": uuid5(NAMESPACE_URL, f"{self.base_url}{path}"),
                }
            )
            content.append(output[:-1])

        path = self.html_output_path.joinpath("feed", "atom.xml")
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(
            self.templates["atom"].substitute(
                {
                    "base_url": self.base_url,
                    "content": "\n".join(content),
                    "title": self.title,
                    "updated_date": datetime.today().isoformat(),
                }
            )
        )

    def generate_rss_feed(self, max_articles: int = 10):
        """Generate the RSS XML feed from a specific amount of articles

        Parameters
        ----------
        max_articles : int
             The number of articles to show in the Rss feed
        """

        articles = sorted(
            self.content_path.joinpath("articles").glob("2*.gmi"),
            reverse=True,
        )

        content = []
        for path in articles[0:max_articles]:
            rel_path = path.relative_to(self.content_path)
            renderer = self.html_render.render(
                rel_path, path.read_text().splitlines()
            )

            if (article_date := self.get_article_datetime(path.name)) is None:
                article_date = renderer["date"]

            slug = renderer["slug"]
            path = rel_path.with_name(f"{slug}.html")

            output = self.templates["rss-entry"].substitute(
                {
                    "author": self.author,
                    "base_url": self.base_url,
                    "body": escape(renderer["body"]),
                    "date": article_date.ctime(),
                    "slug": slug,
                    "title": renderer["title"],
                    "uuid": uuid5(NAMESPACE_URL, f"{self.base_url}{path}"),
                }
            )
            content.append(output[:-1])

        path = self.html_output_path.joinpath("feed", "rss.xml")
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(
            self.templates["rss"].substitute(
                {
                    "base_url": self.base_url,
                    "content": "\n".join(content),
                    "description": "",
                    "title": self.title,
                    "updated_date": datetime.today().ctime(),
                }
            )
        )

    def get_article_datetime(self, article_basename: str) -> datetime | None:
        """Retrieve the datetime from the CSV file

        Notes
        -----
        This method was added to have the full datetime with the Atom and RSS
        XML feeds.

        Parameters
        ----------
        article_basename : str
            The basename of the article path to find in the CSV file

        Returns
        -------
        datetime.datetime or None
            The datetime object instance if the basename exists, None otherwise
        """

        return self.datetimes.get(article_basename, None)
