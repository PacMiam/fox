# Copyleft 2024 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser, BooleanOptionalAction
from datetime import date
from errno import EACCES, EINTR, ENOENT
from importlib import resources
from importlib.metadata import metadata
from locale import LC_ALL, setlocale
from logging import basicConfig, getLogger
from os import getenv
from pathlib import Path
from shutil import copytree, rmtree

from fox import HTML_COLORS, Taniere


def main():
    fox_metadata = metadata(__package__)
    html_colors = ",".join(HTML_COLORS)

    parser = ArgumentParser(
        prog=fox_metadata["Name"],
        description=fox_metadata["Summary"],
        epilog="Copyleft 2024 PacMiam",
    )

    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        default=False,
        help="activate the debug level for the logger",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        default=False,
        help="force the removing of the output directory if already exists",
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        help="output version information and exit",
        version=f"{fox_metadata['Name']} {fox_metadata['Version']}",
    )

    generator_parser = parser.add_argument_group("generator")
    generator_parser.add_argument(
        "-b",
        "--base-url",
        default="/",
        help="the base URL used for inner links",
    )
    generator_parser.add_argument(
        "-g",
        "--gemini-url",
        default=None,
        help=(
            "the GEMINI URL used for permalinks section if different "
            "from base-url"
        ),
    )
    generator_parser.add_argument(
        "-c",
        "--content",
        default="content",
        help="the directory which contains the Gemini files",
        type=Path,
    )
    generator_parser.add_argument(
        "-o",
        "--output",
        default="output",
        help="the directory where the HTML files will be generated",
        type=Path,
    )
    generator_parser.add_argument(
        "-t",
        "--template",
        default=resources.files("fox").joinpath("templates"),
        help="the directory which contains the HTML template files",
        type=Path,
    )
    generator_parser.add_argument(
        "-l",
        "--locale",
        default=getenv("LC_ALL", "fr_FR.utf-8"),
        help="the locale used for date formatting",
    )
    generator_parser.add_argument(
        "-s",
        "--pygments-style",
        default="default",
        help="the style to use with Pygments if this module is active",
    )
    generator_parser.add_argument(
        "--html-colors",
        default=html_colors,
        help=(
            "the list of 4 colors separated with a comma for the template "
            f"(default: {html_colors})"
        ),
    )
    generator_parser.add_argument(
        "--atom",
        action=BooleanOptionalAction,
        default=False,
        help="activate the Atom feed generation (disabled by default)",
    )
    generator_parser.add_argument(
        "--rss",
        action=BooleanOptionalAction,
        default=False,
        help="activate the RSS feed generation (disabled by default)",
    )

    metadata_parser = parser.add_argument_group("metadata")
    metadata_parser.add_argument(
        "--author",
        default="Fox",
        help="the author of this website",
    )
    metadata_parser.add_argument(
        "--datetime-csv",
        default=None,
        help="the file which contains the full datetime of each articles",
        type=Path,
    )
    metadata_parser.add_argument(
        "--description",
        default="The website of the Fox",
        help="the description of this website",
    )
    metadata_parser.add_argument(
        "--keywords",
        default="fox,html,gemini",
        help="the keywords of this website",
    )
    metadata_parser.add_argument(
        "--title",
        default="Fox Website",
        help="the title of this website",
    )
    metadata_parser.add_argument(
        "--year",
        default=date.today().year,
        help="the latest year of publication of this website",
    )

    args = parser.parse_args()

    logger = getLogger(__name__)
    basicConfig(
        format="%(levelname)-5s | %(message)s", level=10 if args.debug else 20
    )

    try:
        setlocale(LC_ALL, args.locale)

        if args.output.exists():
            if not args.force:
                logger.error(
                    "The output directory already exists. Fox do not erase it "
                    "by default. You can use the '--force' flag to force the "
                    "deletion if you know what you are doing."
                )
                return EACCES

            logger.info(
                f"Remove the previous content of the directory “{args.output}”"
            )
            rmtree(args.output)

        taniere = Taniere(
            base_url=args.base_url,
            gemini_url=args.gemini_url,
            content_path=args.content,
            output_path=args.output,
            template_path=args.template,
            style=args.pygments_style,
            author=args.author,
            datetime_path=args.datetime_csv,
            description=args.description,
            keywords=args.keywords,
            title=args.title,
            year=args.year,
            html_colors=[
                element.strip() for element in args.html_colors.split(",")
            ],
            enable_atom=args.atom,
            enable_rss=args.rss,
        )

        logger.info(f"Start generation from the directory “{args.content}”")
        for path in taniere:
            logger.debug(f"Generate {path}")
            taniere.generate_page(path)

        if args.atom:
            logger.info("Generate the Atom XML file")
            taniere.generate_atom_feed()
        if args.rss:
            logger.info("Generate the RSS XML file")
            taniere.generate_rss_feed()

        images_path = args.content.joinpath("images")
        if images_path.exists():
            logger.info("Copy the images directory to the output directory")
            copytree(images_path, args.output.joinpath("html", "images"))
            copytree(images_path, args.output.joinpath("gemini", "images"))

        logger.info(
            f"The website was generated in the directory “{args.output}”"
        )

    except FileNotFoundError as error:
        logger.error(error)
        return ENOENT

    except Exception as error:
        logger.error(error)
        return EINTR


if __name__ == "__main__":
    main()
